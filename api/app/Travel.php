<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    protected $fillable = ['id', 'code', 'seats_amount', 'destiny', 'departore_place', 'price'];

    protected $hidden = ['travel_client_many'];

    public function travels()
    {
        return $this->hasMany('App\ClientTravel');
    }

    public function travel_client_many()
    {
        return $this->hasManyThrough('App\Client', 'App\ClientTravel', 'client_id', 'id', 'id', 'travel_id');
    }
}
