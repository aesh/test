<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['id', 'dni', 'name', 'direction', 'phone'];

    protected $hidden = ['client_travel_many'];

    public function travels()
    {
        return $this->hasMany('App\ClientTravel');
    }

    public function client_travel_many()
    {
        return $this->hasManyThrough('App\Travel', 'App\ClientTravel', 'client_id', 'id', 'id', 'travel_id');
    }
}
