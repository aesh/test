<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTravel extends Model
{
    protected $fillable = ['id', 'client_id', 'travel_id'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function travel()
    {
        return $this->belongsTo('App\Travel');
    }
}
