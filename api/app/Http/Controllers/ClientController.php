<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();
        return response()->json(['payload' => $clients], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'dni'       => 'required|max:100|min:2|unique:clients',
            'name'      => 'required|max:190|min:2',
            'direction' => 'required|max:190|min:2',
            'phone'     => 'required|max:15|min:10',
        ];

        $messages = [
            'dni.unique'         => 'La cedula ya fue usada',
            'dni.min'            => 'La cedula debe tener minimo 2 caracteres',
            'dni.max'            => 'La cedula debe tener maximo 100 caracteres',

            'name.min'           => 'El nombre debe tener minimo 2 caracteres',
            'name.max'           => 'El nombre debe tener maximo 100 caracteres',

            'direction.min'      => 'La direccion debe tener minimo 2 caracteres',
            'direction.max'      => 'La direccion debe tener maximo 100 caracteres',

            'phone.min'          => 'El telefono debe tener minimo 10 caracteres',
            'phone.max'          => 'El telefono debe tener maximo 15 caracteres',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        Client::create($request->all());
        return response()->json('Registro Exitoso', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        $data = [
            'payload' => [
                'client' => $client,
                'travels' => $client->client_travel_many
            ]
        ];
        return response()->json(['payload' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $rules = [
            'dni'       => 'max:100|min:2|unique:clients,dni, ' . $client->id,
            'name'      => 'max:190|min:2',
            'direction' => 'max:190|min:2',
            'phone'     => 'max:15|min:10',
        ];

        $messages = [
            'dni.'          => 'La cedula es requerida',
            'dni.unique'    => 'La cedula ya fue usada',
            'dni.min'       => 'La cedula debe tener minimo 2 caracteres',
            'dni.max'       => 'La cedula debe tener maximo 100 caracteres',

            'name.'         => 'El nombre es requerido',
            'name.min'      => 'El nombre debe tener minimo 2 caracteres',
            'name.max'      => 'El nombre debe tener maximo 100 caracteres',

            'direction.'    => 'La direccion es requerid0',
            'direction.min' => 'La direccion debe tener minimo 2 caracteres',
            'direction.max' => 'La direccion debe tener maximo 100 caracteres',

            'phone.'        => 'El telefono es requerido',
            'phone.min'     => 'El telefono debe tener minimo 10 caracteres',
            'phone.max'     => 'El telefono debe tener maximo 15 caracteres',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $fields = $client->getFillable();
        unset($fields[0]);
        foreach ($fields as $field) {
            if ($request->has($field)) {
                $client->$field = $request->$field;
            }
        }
        if (!$client->isDirty()) {
            return response()->json(['error' => 'Debe especificar un valor diferente para actualizar'], 400);
        }

        $client->save();
        return response()->json(['Actualizacion Exitoso'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
        return response()->json(['Borrado exitosamente'], 200);
    }
}
