<?php

namespace App\Http\Controllers;

use App\Travel;
use Illuminate\Http\Request;
use Validator;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $travels = Travel::all();
        return response()->json(['payload' => $travels], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code'            => 'required|max:190|min:2|unique:travels',
            'seats_amount'    => 'required|integer',
            'destiny'         => 'required|max:190|min:2',
            'departore_place' => 'required|max:190|min:2',
            'price'           => 'required|numeric',
        ];

        $messages = [
            'code.required'         => 'El codigo es requerido',
            'code.unique'           => 'El codigo ya fue usado',
            'code.min'              => 'El codigo debe tener minimo 2 caracteres',
            'code.max'              => 'El codigo debe tener maximo 190 caracteres',

            'seats_amount.required' => 'Las plazas son requeridas',
            'seats_amount.integer'  => 'Las plazas deben ser en numero',

            'destiny.required'      => 'El destino es requerido',
            'destiny.min'           => 'El destino debe tener minimo 2 caracteres',
            'destiny.max'           => 'El destino debe tener maximo 190 caracteres',

            'price.required'        => 'El precio es requerido',
            'price.numeric'         => 'El precio debe ser numerico',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        Travel::create($request->all());
        return response()->json('Registro Exitoso', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function show(Travel $travel)
    {

        $data = [
            'payload' => [
                'travel'  => $travel,
                'clients' => $travel->travel_client_many,
            ],
        ];
        return response()->json(['payload' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function edit(Travel $travel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Travel $travel)
    {
        $rules = [
            'code'            => 'max:100|min:2|unique:travels,code, ' . $travel->id,
            'seats_amount'    => 'integer',
            'destiny'         => 'max:190|min:2',
            'departore_place' => 'max:190|min:2',
            'price'           => 'numeric',
        ];

        $messages = [
            'code.unique'          => 'El codigo ya fue usado',
            'code.min'             => 'El codigo debe tener minimo 2 caracteres',
            'code.max'             => 'El codigo debe tener maximo 190 caracteres',

            'seats_amount.integer' => 'Las plazas deben ser en numero',

            'destiny.min'          => 'El destino debe tener minimo 2 caracteres',
            'destiny.max'          => 'El destino debe tener maximo 190 caracteres',

            'price.numeric'        => 'El precio debe ser numerico',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $fields = $travel->getFillable();
        unset($fields[0]);
        foreach ($fields as $field) {
            if ($request->has($field)) {

                $travel->$field = $request->$field;
            }
        }
        if (!$travel->isDirty()) {
            return response()->json(['error' => 'Debe especificar un valor diferente para actualizar'], 400);
        }

        $travel->save();
        return response()->json(['Actualizacion Exitoso'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Travel $travel)
    {
        $travel->delete();
        return response()->json(['Borrado exitosamente'], 200);
    }
}
