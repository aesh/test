<?php

namespace App\Http\Controllers;

use App\ClientTravel;
use Illuminate\Http\Request;
use Validator;

class ClientTravelController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'client_id' => 'required|integer|exists:clients,id',
            'travel_id' => 'required|integer|exists:travels,id',
        ];

        $messages = [
            'client_id.required' => 'El id del cliente es requerido',
            'client_id.integer'  => 'Error con el id del cliente',
            'client_id.exists'   => 'No se encontro el cliente',

            'travel_id.required' => 'El id del viaje es requerdo',
            'travel_id.integer'  => 'Error con el id del viaje',
            'travel_id.exists'   => 'No se encontro el viaje',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        ClientTravel::create($request->all());
        return response()->json('Registro Exitoso', 200);
    }
}
