<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'dni'       => uniqid(),
        'name'      => $faker->name,
        'direction' => $faker->address,
        'phone'     => $faker->phoneNumber,
    ];
});

$factory->define(App\Travel::class, function (Faker $faker) {
    return [
        'code'            => uniqid(),
        'seats_amount'    => $faker->numberBetween(10, 100),
        'destiny'         => $faker->city,
        'departore_place' => $faker->address,
        'price'           => $faker->numberBetween(100, 10000),
    ];
});

$factory->define(App\ClientTravel::class, function (Faker $faker) {
    $clients = App\Client::all();
    $travels = App\Travel::all();
    return [
        'client_id' => $clients->random()->id,
        'travel_id' => $travels->random()->id,
    ];
});
