<?php

use App\ClientTravel;
use Illuminate\Database\Seeder;

class ClientTravelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ClientTravel::class, 100)->create();
    }
}
