# TEST STRAPPINC

sistema de prueba para la compañia strappinc, el desarrollo se basa en dos sistemas
un api creada en laravel 5.6 y una app desarrollada con react native junto a expo.

los pasos de instalacion son sencillos:
## Clone el repositorio
    una vez clonado el respositorio tendra dos carpetas una llamada api y la otra llamada app
    aqui los pasos para cada una de las carpetas:
    
### API
        **# NOTA: DEBE TENER INSTALADO COMPOSER**
        1. ingresar mediante consola / terminal para poder instalar las dependencias necesarias.
        
        2. ingresar el comando **composer install** este comando instalara todas las dependencias necesarias de laravel
        
        3. luego se debe ingresar **php artisan key:generate** esto le creara un archivo **.env** de configuracion la cual va a permitir tener
        una configuracion global.
        
        4. dentro del archivo **.env** debe poner los acessos a su base de 
        datos tales como
            1. DB_CONNECTION=mysql
            2. DB_HOST=127.0.0.1
            3. DB_PORT=3306
            4. DB_DATABASE=example
            5. DB_USERNAME=example
            6. DB_PASSWORD=example
            
        5. una vez configurado los datos de la base de datos, puede ingresar dos comando (puede ser uno o el otro), los cuales son:
            1.**php artisan migrate:fresh --seed**: este comando instalara las 
            tablas, creara las relaciones y ademas creara unos datos de prueba 
            de esa manera tendra datos de prueba ya listo
            2. **php artisan migrate**: este comando instalara las tablas, 
            creara las relaciones pero no ingresara datos de prueba.
            
        6. ya realizado los pasos anteriores, faltaria desplegar el proyecto el cual lo puede hacer de dos formas:
            1. Crear un virtual host para que apunte a la carpeta 
            **raiz_del_proyecto/public/**, e ingresar mediante su virtual host.
            2. ejecutar el siguiente comando **php artisan serve** esto creara
            un puerto con su ip de localhost con el puerto 8000 o el puerto libre
             que pueda conseguir, con esto se ahorra crear un virtual host
             
        7. ya siguiendo estos pasos tendra lista y corriendo su api. :) 
        
        La ruta base para las pruebas del api es la siguiente: 
        **www.ruta_del_proyecto.com/api/**
        
        Las rutas creadas para que puedas experimentar son las siguientes:
        +--------+-----------+-------------+--------------------------------+
        |Method    |    URI                |Description                     |
        +--------+-----------+-------------+--------------------------------+
        |POST      | api/client/travel/new |agregar nuevo viaje al cliente  |
        |GET|HEAD  | api/clients           |obtener lista de clientes       |
        |POST      | api/clients           |agregar nuevo cliente           |
        |GET|HEAD  | api/clients/{client}  |obtener cliente especifico      |
        |PUT|PATCH | api/clients/{client}  |actualizar cliente              |
        |DELETE    | api/clients/{client}  |borrar un cliente               |
        |GET|HEAD  | api/travels           |obtener viajes                  |
        |POST      | api/travels           |agregar viaje                   |
        |GET|HEAD  | api/travels/{travel}  |obtener viaje en especifico     |
        |PUT|PATCH | api/travels/{travel}  |actualizar viaje                |
        |DELETE    | api/travels/{travel}  |borrar viaje                    |
        +--------+-----------+-------------+--------------------------------+
        
        de igual manera puede ver todas las rutas disponibles mediante un comando
        **php artisan route:list**
        
### APP
        1. ingresar mediante consola / terminal para poder instalar las dependencias necesarias
        
        2. se debe correr el siguiente comando para poder usar **EXPO**:  **npm install exp --global**
        
        3. ejecutar el comando **npm install** esto instalara todas las dependecias necesarias
        
        4. luego ejecutar **exp start** esto desplegara una informacion lo mas recomandado seria utiliazar la aplicacion movil de expo 
        para de esa manera poder visualizar mejor la app **https://play.google.com/store/apps/details?id=host.exp.exponent&hl=es**,
        o instalar su aplicacion para escritorio **https://expo.io/learn**
        
        5. si descargo la aplicacion movil solo tiene que abrirla y escanear el codigo QR generado en la terminal 
        (deben estar conectados a la misma red)
        
        6. si descargo la aplicacion para escritorio abra la IDE de Expo y agregue el proyecto y corrarlo, na vez corriendo podra seleccionar
        o abrir la aplicacion en un simulador de android/IOS, o compartirla mediante un codigo QR.
        
        7. puede configurarlo con tres opciones
            1. Tunnel
            2. Lan => recomiendo esta para realizar las pruebas y deben estar 
            conectados en la misma red.
            3. Localhost
            
**cualquier inconveniente pueden avisarme o mandar un correo aesh1415.as@gmail.com y con gusto los  ayudare :)**
## EL ÉXITO ESTÁ EN LA PERSEVERANCIA. Aleverarise.