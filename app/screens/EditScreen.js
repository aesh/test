import React from "react";
import { View, StyleSheet, ActivityIndicator, ScrollView } from "react-native";
import { httpGetPost, httpUpdatePost } from "../utils/http";
import Toast from 'react-native-simple-toast';
import { options, Post } from "../utils/forms/post";
import t from 'tcomb-form-native';
import { Button, Card } from "react-native-elements";
const Form = t.form.Form;

export default class EditScreen extends React.Component {
	
	constructor(props) {
		super(props);
		
		//EditScreen states 
		this.state = {
			post: {
                title: '',
                body: '',
                userId: '',
            },
            loaded: true
		}
	}
	
	//the screen title
	static navigationOptions = {
		title: "Editing Post"
    };
	
	//Component Lifecycle ,
    componentDidMount() {
		this.onGetData()
	}

	//post information
	onGetData = () => {
		postId = this.props.navigation.getParam('id', 0)
		httpGetPost(postId)
			.then((response) => {
				this.setState({
					post: response.post,
					loaded: false,
				})
			})
			.catch((error) => console.error(error))
	}

	//this function allows you to update a post
	onUpdatePost = () => {
		const validate = this.refs.form.getValue();
		if (validate) {
			const {id, title, body, userId} = this.state.post
			httpUpdatePost(id, title, body, userId)
				.then(() => {
                    Toast.show('Updated Successfully.', Toast.LONG)
                    this.props.navigation.navigate('Home')
				})
				.catch(() => {
					Toast.show('There was a mistake. Please try again.')
				})
		}
	}

	//change values states
	onChange (post){
        this.setState({post});
    }

	render() {
        const {post} = this.state
        
        if (this.state.loaded) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
        }

		return (
			<View style={styles.container}>
                <ScrollView>
                    <Card title="Update Post" >
                        <Form 
                            ref="form"
                            type={Post}
                            options={options}
                            value={post}
                            onChange={ (v) => this.onChange(v) }
                        />
                        <Button
                            buttonStyle={styles.buttomAdd}
                            onPress={this.onUpdatePost}
                            leftIcon={{name: 'add'}}
                            title='Update' />
                    </Card>
                </ScrollView>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
	},
	buttomAdd: {
		backgroundColor: '#2195F3',
		marginTop: 20
	},
});
