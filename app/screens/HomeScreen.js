import React from "react";
import { ScrollView, StyleSheet, View, ActivityIndicator } from "react-native";
import { ListItem, Icon, FormInput, FormLabel } from "react-native-elements";
import { httpGetData } from "../utils/http";
import Toast from 'react-native-simple-toast';

export default class HomeScreen extends React.Component {
    
    constructor(props) {
        super(props);
        
        //HomeScreen states
        this.state = {
            posts: [],
            userId: '',
            loaded: true
        }
    }
    
    //the screen title
    static navigationOptions = {
        headerTitle: "Posts"
    };

    //Component Lifecycle ,
    componentDidMount() {
        httpGetData()
            .then(response => this.setState({posts: response.data, loaded: false}))
            .catch(error => console.error(error));
    }

    //this function allows to search any post for userId
    onSearch = (userId) => {
        this.setState({loaded: true})
        this.setState({userId}, () => {
            httpGetData(this.state.userId)
            .then(response => {
                if (response.data.length > 0) {
                    this.setState({posts: response.data, loaded: false})
                }else{
                    this.setState({posts: response.data, loaded: false})
                    Toast.show(`Results not found with this UserId: ${this.state.userId}.`, Toast.LONG)
                }
            })
            .catch(error => console.error(error));
        })
    }

    //
    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    style={styles.container}
                    contentContainerStyle={styles.contentContainer}
                >
                    <FormLabel>User Id</FormLabel>
                    <FormInput 
                        inputStyle={styles.search}
                        onChangeText={(text) => this.onSearch(text) }
                        value={this.state.userId} />

                    {
                        (this.state.loaded) ?
                            <View style={styles.preloader}>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>
                        :
                        this.state.posts.map((currentValue, index, array) => (
                            <ListItem
                                key={index}
                                leftIcon={
                                    <Icon
                                    name='archive' />
                                }
                                title={currentValue.title}
                                subtitle={currentValue.body}
                                onPress={() =>
                                    this.props.navigation.navigate("Show", {
                                        id: currentValue.id,
                                        userId: currentValue.userId
                                    })
                                }
                            />
                        ))
                    }

                    
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    developmentModeText: {
        marginBottom: 20,
        color: "rgba(0,0,0,0.4)",
        fontSize: 14,
        lineHeight: 19,
        textAlign: "center"
    },
    contentContainer: {
        paddingTop: 30
    },
    preloader: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    search: {
        marginBottom: 30
    }
});
