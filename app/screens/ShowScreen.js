import React from "react";
import { ScrollView, View, StyleSheet, ActivityIndicator } from "react-native";
import { httpGetPost, httpDeletePost } from "../utils/http";
import { Card, Text, Button, Divider } from "react-native-elements";
import Toast from 'react-native-simple-toast';

export default class ShowScreen extends React.Component {
	
	constructor(props) {
		super(props);
		
		//NewScreen states
		this.state = {
			post: {},
			user: {},
			comments: {}, 
			loaded: true
		}
	}

	//Component Lifecycle ,
	componentDidMount() {
		this.onGetData()
	}

	//post information
	onGetData = () => {
		postId = this.props.navigation.getParam('id', 0)
		httpGetPost(postId)
			.then((response) => {
				this.setState({
					post: response.post,
					user: response.user,
					comments: response.comments,
					loaded: false,
				}, () => {
					console.log(this.state.comments)
				})
			})
			.catch((error) => console.error(error))
	}

	//this function allows you to delete a post
	onDelete = () => {
		httpDeletePost(this.state.post.id)
			.then(() => {
				Toast.show('Deleted Successfully.', Toast.LONG)
				this.props.navigation.navigate('Home')
			})
			.catch(() => Toast.show('There was a mistake. Please try again.', Toast.LONG))
	}
	
	//the screen title
	static navigationOptions = {
		title: 'Back'
	};

	render() {
		if (this.state.loaded) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            )
		}
		
		return (
			<View style={styles.container}>
				<ScrollView>
					<Card
						title={this.state.user.name}
						image={require('./../assets/images/post.png')}>
						<Text style={styles.title}>
							{this.state.post.title}
						</Text>
						<Text style={styles.body}>
							{this.state.post.body}
						</Text>
						<View style={styles.viewButtons}>
							<Button
								backgroundColor="#009588"
								borderRadius={20}
								buttonStyle={styles.button}
								rightIcon={{name: 'edit'}}
								onPress={() =>
									this.props.navigation.navigate("Edit", {
										id: this.state.post.id,
									})
								}
								title='EDIT' />

							<Button
								backgroundColor="#FF5622"
								borderRadius={20}
								onPress={this.onDelete}
								buttonStyle={styles.button}
								rightIcon={{name: 'delete'}}
								title='DELETE' />
						</View>
					</Card>

					<Card
						title="Comments">
						{this.state.comments.map((currentValue, index, array) => {
							return (
								<View key={index}  style={styles.viewComments}>
									<Text style={styles.commentName}>
										{currentValue.name}
									</Text>
									<Text style={styles.commentEmail}>
										{currentValue.email}
									</Text>
									<Text style={styles.commentBody}>
										{currentValue.body}
									</Text>
									<Divider style={{ backgroundColor: '#009588' }} />
								</View>
							)
						})}
					</Card>

				</ScrollView>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
	},
	buttomAdd: {
		backgroundColor: '#2195F3',
		marginTop: 20
	},
	title: {
		fontSize: 20,
		textAlign: 'justify',
		marginBottom: 10
	},
	preloader: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
	},
	viewButtons: {
		marginTop: 20,
	},
	button: {
		marginTop: 10,
	},
	viewComments: {
		padding: 7
	},
	commentName: {
		fontSize: 18,
		marginBottom: 10
	},
	commentEmail: {
		fontSize: 15,
		paddingLeft: 10
	},
	commentBody: {
		fontSize: 13,
		padding: 10,
		textAlign: 'justify'
	}
});
