import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { httpNewPost } from "../utils/http";
import Toast from 'react-native-simple-toast';
import { options, Post } from "../utils/forms/post";
import t from 'tcomb-form-native';
import { Button, Card } from "react-native-elements";
const Form = t.form.Form;

export default class NewScreen extends React.Component {
	
	constructor(props) {
		super(props);
		
		//NewScreen states 
		this.state = {
			post: {
                title: '',
                body: '',
                userId: '',
            }
		}
	}
	
	//the screen title
	static navigationOptions = {
		title: "New Post"
	};

	//this function allows you to new a post
	onNewPost = () => {
		const validate = this.refs.form.getValue();
		if (validate) {
			const {title, body, userId} = this.state.post
			httpNewPost(title, body, userId)
				.then(() => {
					this.setState({
						post: {
							title: '',
							body: '',
							userId: '',
						}
					}, () => {
						Toast.show('Successful registration.', Toast.LONG)
						this.props.navigation.navigate('Home')
					})
				})
				.catch(() => {
					Toast.show('There was a mistake. Please try again.')
				})
		}
	}

	//change values states
	onChange (post){
        this.setState({post});
    }

	render() {
		const {post} = this.state
		return (
			<View style={styles.container}>
				<ScrollView>
					<Card title="Add Post" >
						<Form 
							ref="form"
							type={Post}
							options={options}
							value={post}
							onChange={ (v) => this.onChange(v) }
						/>
						<Button
							buttonStyle={styles.buttomAdd}
							onPress={this.onNewPost}
							leftIcon={{name: 'add'}}
							title='Add Post' />
					</Card>
				</ScrollView>
            </View>
		);
	}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
	},
	buttomAdd: {
		backgroundColor: '#2195F3',
		marginTop: 20
	},
});
