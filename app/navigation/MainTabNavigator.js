import React from "react";
import { Platform } from "react-native";
import {
    createStackNavigator,
    createBottomTabNavigator
} from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import HomeScreen from "../screens/HomeScreen";
import NewScreen from "../screens/NewScreen";
import ShowScreen from "../screens/ShowScreen";
import EditScreen from "../screens/EditScreen";

//screen and routes for navigate in the aplication 

//tab Home configuration
const HomeStack = createStackNavigator({
    Home: HomeScreen,
    Show: ShowScreen,
    Edit: EditScreen
});
HomeStack.navigationOptions = {
	tabBarLabel: "Home",
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === "ios"
                    ? `ios-information-circle${focused ? "" : "-outline"}`
                    : "ios-easel"
            }
        />
    )
};

//tab new configuration
const NewStack = createStackNavigator({
    New: NewScreen
});
NewStack.navigationOptions = {
    tabBarLabel: "New",
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === "ios"
                    ? `ios-link${focused ? "" : "-outline"}`
                    : "ios-add-circle"
            }
        />
    )
};

export default createBottomTabNavigator({
    HomeStack,
    NewStack,
});