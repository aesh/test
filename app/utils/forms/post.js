/*
* forms configuration to add and update
*/

import t from 'tcomb-form-native';
const Form = t.form.Form;

//fields and validations for the add and update forms  
export const Post = t.struct({
    title: t.String,
    body: t.String,
    userId: t.Number,
});

//forms options
export const options = {
    fields: {
        title: {
            label: 'Title (*)',
            placeholder: 'Title'
        },
        userId: {
            label: 'User ID (*)',
            help: 'User ID',
            config: {
                step:1,
            },
        },
        body: {
            label: 'Body (*)',
            placeholder: 'Body',
            multiline: true,
            stylesheet: {
                ...Form.stylesheet,
                textbox:{
                    ...Form.stylesheet.textbox,
                    normal: {
                        ...Form.stylesheet.textbox.normal,
                        height: 75
                    },
                    error: {
                        ...Form.stylesheet.textbox.error,
                        height: 75
                    }
                }
            }
        }
    }
}