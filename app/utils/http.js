import axios from "axios";
import { ROOT_API } from "./url";

//axios instance.
const http = headers =>
    axios.create({
        baseURL: ROOT_API
    });

//function to get data
const httpGetData = (id = null) => {
    return new Promise((resolve, reject) => {
        if (id) {
            http()
                .get("/posts?userId=" + id)
                .then(response => resolve(response))
                .catch(error => reject(error));
        }else{
            http()
                .get("/posts")
                .then(response => resolve(response))
                .catch(error => reject(error));
        }
    });
};

/*
* this function allows you to add a new post
* these following params must be sent 
* title => string
* body => string
* userId => integer
*/
const httpNewPost = (title, body, userId) => {
    return new Promise((resolve, reject) => {
        http()
            .post("/posts", {
                title: title,
                body: body,
                userId: userId
            })
            .then(response => resolve(response))
            .catch(error => reject(error));
    });
};

/*
* this function allows you to show a post
* with the user and comments 
* the postId must be sent 
*/
const httpGetPost = (postId) => {
    return new Promise((resolve, reject) => {
        const data = {
            post: {},
            user: {},
            comments: {}
        }
        http()
            .get("/posts/" + postId)
            .then((response) => {
                data.post = response.data
                http()
                    .get("/users/" + data.post.userId)
                    .then(response => {
                        data.user = response.data
                        http()
                            .get("/posts/" + data.post.id + "/comments")
                            .then(response => {
                                data.comments = response.data
                                resolve(data)
                            })
                    })
                    .catch(error => reject(error));
            })
            .catch(error => reject(error));
    });
};

/*
* this function allows you to update a post
* the postId must be sent 
*/
const httpUpdatePost = (id, title, body, userId) => {
    return new Promise((resolve, reject) => {
        http()
            .put("/posts/" + id, {
                title: title,
                body: body,
                userId: userId
            })
            .then(response => resolve(response))
            .catch(error => reject(error));
    });
};

/*
* this function allows you to delete a post
* the postId must be sent 
*/
const httpDeletePost = (id) => {
    return new Promise((resolve, reject) => {
        http()
            .delete("/posts/" + id)
            .then(response => resolve(response))
            .catch(error => reject(error));
    });
};

export default http;
export {
    httpGetData,
    httpNewPost,
    httpGetPost,
    httpUpdatePost,
    httpDeletePost
};
